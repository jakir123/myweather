package com.cse24gmail.jakir.myweather.data;

import org.json.JSONObject;

/**
 * Created by Jakir on 6/7/2015.
 */
public class Item implements JSONPopulator{
    private Condition condition;

    public Condition getCondition() {
        return condition;
    }

    @Override
    public void populate(JSONObject data) {
        condition=new Condition();
        condition.populate(data.optJSONObject("condition"));

    }
}
