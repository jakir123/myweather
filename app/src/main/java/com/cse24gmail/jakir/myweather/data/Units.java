package com.cse24gmail.jakir.myweather.data;

import org.json.JSONObject;

/**
 * Created by Jakir on 6/7/2015.
 */
public class Units implements JSONPopulator {
    private String temperature;

    public String getTemperature() {
        return temperature;
    }

    @Override
    public void populate(JSONObject data) {
        temperature=data.optString("temperature");
    }
}
