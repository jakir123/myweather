package com.cse24gmail.jakir.myweather.service;

import com.cse24gmail.jakir.myweather.data.Channel;

/**
 * Created by Jakir on 6/7/2015.
 */
public interface WeatherServiceCallback {
    void serviceSuccess(Channel channel);

    void serviceFailure(Exception exception);
}
