package com.cse24gmail.jakir.myweather.data;

import org.json.JSONObject;

/**
 * Created by Jakir on 6/7/2015.
 */
public interface JSONPopulator {
    void populate(JSONObject jsonObj);
}
