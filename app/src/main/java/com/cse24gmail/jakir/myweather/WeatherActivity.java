package com.cse24gmail.jakir.myweather;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cse24gmail.jakir.myweather.data.Channel;
import com.cse24gmail.jakir.myweather.data.Item;
import com.cse24gmail.jakir.myweather.service.WeatherServiceCallback;
import com.cse24gmail.jakir.myweather.service.YahooWeatherService;

public class WeatherActivity extends ActionBarActivity implements WeatherServiceCallback {
    private TextView tvTemperature;
    private TextView tvCondition;
    private TextView tvLocation;
    private ImageView imvWeatherIcon;

    private ProgressDialog dialog;

    private YahooWeatherService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        initialization();

    }

    private void initialization() {
        tvTemperature= (TextView) findViewById(R.id.tvTemp);
        tvCondition= (TextView) findViewById(R.id.tvCondition);
        tvLocation= (TextView) findViewById(R.id.tvLocation);
        imvWeatherIcon= (ImageView) findViewById(R.id.imvWeatherIcon);
        service=new YahooWeatherService(this);
        dialog=new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

        refreshWeather();
    }

    private void refreshWeather() {
        service.refreshWeather("Dhaka, Bangladesh");
    }

    @Override
    public void serviceSuccess(Channel channel) {
        dialog.hide();
        Item item = channel.getItem();
        int resourceId = getResources().getIdentifier("drawable/icon_" + item.getCondition().getCode(), null, getPackageName());

        @SuppressWarnings("deprecation")
        Drawable weatherIconDrawble = getResources().getDrawable(resourceId);
        imvWeatherIcon.setImageDrawable(weatherIconDrawble);
        tvTemperature.setText(item.getCondition().getTemperature() + "\u00B0" + channel.getUnits().getTemperature());
        tvCondition.setText(item.getCondition().getDescription());
        tvLocation.setText(service.getLocation());


    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(getApplicationContext(),exception.getMessage(),Toast.LENGTH_LONG).show();
    }
}
